# Unique Jobs
The unique tasks feature in Asynq make it simple to ensure that you have only one copy of a task enqueued in Redis.
This feature is useful when you want to deduplicate tasks to ensure that you are not creating redundant work.

- There are two ways you can go about ensuring the uniqueness of tasks with Asynq.
## Overview
- - Using TaskID option: Generate a unique task ID on your own
- - Using Unique option: Let Asynq create a uniquness lock for the task

## Using Task Id Approach
If you could go with the first approach, it's guaranteed that at any moment there's only one task with a given task ID. If you try to enqueue another task with the same task ID, you'll get ErrTaskIDConflict error.

## Using Unique option
The second approach is based on uniqueness locks. When enqueueing a task with Unique option, Client checks whether if it can acquire a lock for the given task. The task is enqueued only if the lock can be acquired. If there's already another task holding the lock, then the Client will return an error (See example code below on how to inspect the error).

The uniqueness lock has a TTL associated with it to avoid holding the lock forever. The lock will be released after the TTL or if the task holding the lock gets processed successfully before the TTL.
One important thing to note is that the Asynq's unique task feature is best-effort uniqueness. In other words, it's possible to enqueue a duplicate task if the lock has expired before the task gets processed.

The uniqueness of a task is based on the following properties:

- Type
- Payload
- Queue

So if there's a task with the same type and payload, and if it's enqueued to the same queue, then another task with these same properties won't be enqueued until the lock has been released.

_**Refer the examples in the unique-tasks directory**_

# Inspect and Mutate Queues and Tasks
## Using the Inspector Client Interface
Inspector is a client interface to inspect and mutate the state of queues and tasks.
### Creating a New Inspector
func NewInspector(r RedisConnOpt) *Inspector
```go
    redisConnOpt := asynq.RedisClientOpt{
		Addr: "localhost:6379",
	}
	inspector := asynq.NewInspector(redisConnOpt)

```
### Methods Provided to Mututate States and Tasks
- func NewInspector(r RedisConnOpt) *Inspector
- func (i *Inspector) ArchiveAllAggregatingTasks(queue, group string) (int, error)
- func (i *Inspector) ArchiveAllPendingTasks(queue string) (int, error)
- func (i *Inspector) ArchiveAllRetryTasks(queue string) (int, error)
- func (i *Inspector) ArchiveAllScheduledTasks(queue string) (int, error)
- func (i *Inspector) ArchiveTask(queue, id string) error
- func (i *Inspector) CancelProcessing(id string) error
- func (i *Inspector) Close() error
- func (i *Inspector) ClusterKeySlot(queue string) (int64, error)
- func (i *Inspector) ClusterNodes(queue string) ([]*ClusterNode, error)
- func (i *Inspector) DeleteAllAggregatingTasks(queue, group string) (int, error)
- func (i *Inspector) DeleteAllArchivedTasks(queue string) (int, error)
- func (i *Inspector) DeleteAllCompletedTasks(queue string) (int, error)
- func (i *Inspector) DeleteAllPendingTasks(queue string) (int, error)
- func (i *Inspector) DeleteAllRetryTasks(queue string) (int, error)
- func (i *Inspector) DeleteAllScheduledTasks(queue string) (int, error)
- func (i *Inspector) DeleteQueue(queue string, force bool) error
- func (i *Inspector) DeleteTask(queue, id string) error
- func (i *Inspector) GetQueueInfo(queue string) (*QueueInfo, error)
- func (i *Inspector) GetTaskInfo(queue, id string) (*TaskInfo, error)
- func (i *Inspector) Groups(queue string) ([]*GroupInfo, error)
- func (i *Inspector) History(queue string, n int) ([]*DailyStats, error)
- func (i *Inspector) ListActiveTasks(queue string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListAggregatingTasks(queue, group string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListArchivedTasks(queue string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListCompletedTasks(queue string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListPendingTasks(queue string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListRetryTasks(queue string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListScheduledTasks(queue string, opts ...ListOption) ([]*TaskInfo, error)
- func (i *Inspector) ListSchedulerEnqueueEvents(entryID string, opts ...ListOption) ([]*SchedulerEnqueueEvent, error)
- func (i *Inspector) PauseQueue(queue string) error
- func (i *Inspector) Queues() ([]string, error)
- func (i *Inspector) RunAllAggregatingTasks(queue, group string) (int, error)
- func (i *Inspector) RunAllArchivedTasks(queue string) (int, error)
- func (i *Inspector) RunAllRetryTasks(queue string) (int, error)
- func (i *Inspector) RunAllScheduledTasks(queue string) (int, error)
- func (i *Inspector) RunTask(queue, id string) error
- func (i *Inspector) SchedulerEntries() ([]*SchedulerEntry, error)
- func (i *Inspector) Servers() ([]*ServerInfo, error)
- func (i *Inspector) UnpauseQueue(queue string) error

_**Refer the example in the inspector-queues-and-tasks directory**_

# Allow aggregating group of tasks to batch multiple successive operations
Task aggregation allows you to enqueue multiple tasks successively, and have them passed to the Handler together rather than individually. The feature allows you to batch multiple successive operations into one.

In order to use the task aggregation feature, you need to enqueue the tasks in the same queue with the common group name. Tasks enqueued with the same (queue, group) pairs are aggregated into one task by GroupAggregator that you provide and the aggregated task will be passed to the handler.

When creating an aggregated task, Asynq server will wait for more tasks until a configurable grace period. The grace period is renewed whenever you enqueue a new task with the same (queue, group).

The grace period has configurable upper bound: you can set a maximum aggregation delay, after which Asynq server will aggregate the tasks regardless of the remaining grace period.

You can also set a maximum number of tasks that can be aggregated together. If that number is reached, Asynq server will aggregate the tasks immediately.

```go
srv := asynq.NewServer(
           redisConnOpt,
           asynq.Config{
               GroupAggregator:  asynq.GroupAggregatorFunc(aggregate),
               GroupMaxDelay:    10 * time.Minute,
               GroupGracePeriod: 2 * time.Minute,
               GroupMaxSize:     20,
               Queues: map[string]int{"notifications": 1},
           },
       )
```
### Batched jobs structure redis
![batched_jobs.png](batched_jobs.png)

**_Refer the example in the batch-jobs directory******_

# Asynq job object structure redis
Following is how a struct job object is stored in redis
![asynq_job.png](asynq_job.png)
