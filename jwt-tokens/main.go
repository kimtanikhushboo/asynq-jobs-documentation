package main

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

var (
	ctx = context.Background()
)

type Keys struct {
	PrivateKey []byte `json:"private_key"`
	PublicKey  []byte `json:"public_key"`
	Token      string `json:"token"`
}

type User struct {
	ID       int
	Username string
}

func main() {
	// Initialize Redis client
	rdb := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})

	// Generate RSA keys
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(fmt.Errorf("error generating RSA key: %v", err))
	}
	publicKey := &privateKey.PublicKey

	// Generate access token
	user := User{
		ID:       1,
		Username: "johndoe",
	}
	tokenString, err := generateToken(user, privateKey)
	if err != nil {
		panic(fmt.Errorf("error generating access token: %v", err))
	}

	// Create Keys struct
	keys := Keys{
		PrivateKey: x509.MarshalPKCS1PrivateKey(privateKey),
		PublicKey:  x509.MarshalPKCS1PublicKey(publicKey),
		Token:      tokenString,
	}

	// Serialize Keys to JSON
	keysJSON, err := json.Marshal(keys)
	if err != nil {
		panic(fmt.Errorf("error encoding Keys to JSON: %v", err))
	}

	// Store Keys JSON in Redis
	if err := rdb.Set(ctx, fmt.Sprintf("user:%d:keys", user.ID), keysJSON, 0).Err(); err != nil {
		panic(fmt.Errorf("error saving keys to Redis: %v", err))
	}
	fmt.Println("Keys saved to Redis for user:", user.ID)

	// Retrieve Keys JSON from Redis
	retrievedKeys, err := getKeys(rdb, user.ID)
	if err != nil {
		panic(fmt.Errorf("error retrieving keys: %v", err))
	}
	fmt.Printf("Retrieved keys for user %d: %+v\n", user.ID, retrievedKeys)

	// Parse and verify access token
	parsedUser, err := parseToken(retrievedKeys.Token, publicKey)
	if err != nil {
		panic(fmt.Errorf("error parsing access token: %v", err))
	}
	fmt.Println("Parsed user:", parsedUser)

	// Delete the keys from Redis
	if err := deleteKeys(rdb, user.ID); err != nil {
		panic(fmt.Errorf("error deleting keys from Redis: %v", err))
	}
	fmt.Println("Keys deleted from Redis for user:", user.ID)
}

func generateToken(user User, privateKey *rsa.PrivateKey) (string, error) {
	claims := jwt.MapClaims{
		"userID":   user.ID,
		"username": user.Username,
		"exp":      time.Now().Add(time.Minute * 5).Unix(), // Token expires in 5 minutes
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	tokenString, err := token.SignedString(privateKey)
	if err != nil {
		return "", fmt.Errorf("error signing token: %v", err)
	}
	return tokenString, nil
}

func parseToken(tokenString string, publicKey *rsa.PublicKey) (*User, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err != nil {
		return nil, fmt.Errorf("error parsing token: %v", err)
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, fmt.Errorf("invalid token")
	}
	userID, ok := claims["userID"].(float64)
	if !ok {
		return nil, fmt.Errorf("invalid user ID in token")
	}
	username, ok := claims["username"].(string)
	if !ok {
		return nil, fmt.Errorf("invalid username in token")
	}
	return &User{ID: int(userID), Username: username}, nil
}

func deleteKeys(rdb *redis.Client, userID int) error {
	if err := rdb.Del(ctx, fmt.Sprintf("user:%d:keys", userID)).Err(); err != nil {
		return fmt.Errorf("error deleting keys from Redis: %v", err)
	}
	return nil
}

func getKeys(rdb *redis.Client, userID int) (*Keys, error) {
	data, err := rdb.Get(ctx, fmt.Sprintf("user:%d:keys", userID)).Bytes()
	if err != nil {
		return nil, fmt.Errorf("error retrieving keys from Redis: %v", err)
	}
	var keys Keys
	if err := json.Unmarshal(data, &keys); err != nil {
		return nil, fmt.Errorf("error decoding Keys from JSON: %v", err)
	}
	return &keys, nil
}
