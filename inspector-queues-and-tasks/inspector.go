package main

import (
	"fmt"
	"github.com/hibiken/asynq"
)

//https://pkg.go.dev/github.com/hibiken/asynq#Inspector : Inspector is a client interface to inspect and mutate the state of queues and tasks.

func main() {
	redisConnOpt := asynq.RedisClientOpt{
		Addr: "localhost:6379",
	}

	inspector := asynq.NewInspector(redisConnOpt)

	deletedCount, err := inspector.DeleteAllScheduledTasks("default")
	if err != nil {
		fmt.Printf("Error deleting scheduled tasks: %v\n", err)
		return
	}
	//func (*Inspector) DeleteTask ¶
	//added in v0.18.0
	//func (i *Inspector) DeleteTask(queue, id string) error
	//DeleteTask deletes a task with the given id from the given queue. The task needs to be in pending, scheduled, retry, or archived state, otherwise DeleteTask will return an error.
	//
	//	If a queue with the given name doesn't exist, it returns an error wrapping ErrQueueNotFound. If a task with the given id doesn't exist in the queue, it returns an error wrapping ErrTaskNotFound. If the task is in active state, it returns a non-nil error.

	fmt.Printf("Deleted %d tasks from the queue '%s'\n", deletedCount, "default")

}
