package main

import (
	"database/sql"
	"encoding/json"
	"github.com/hibiken/asynq"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"log"
)

type CheckSerialization struct {
	BunDb bun.DB
}

// client.go
func main() {
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})
	dsn := "postgres://postgres:abcd1234@localhost:5432/test?sslmode=disable"
	sqlDb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))
	bunDB := bun.NewDB(sqlDb, pgdialect.New())

	obj := CheckSerialization{*bunDB}

	log.Print(obj.BunDb.String())

	payload, err := json.Marshal(obj)
	if err != nil {
		log.Fatal(err)
	}
	t1 := asynq.NewTask("check:serialization", payload)

	// Process the task immediately.
	info, err := client.Enqueue(t1)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

}
