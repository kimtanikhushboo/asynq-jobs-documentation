package main

import (
	"context"
	"encoding/json"
	"github.com/hibiken/asynq"
	"github.com/uptrace/bun"
	"log"
)

type CheckSerialization struct {
	BunDb bun.DB
}

// workers.go
func main() {
	srv := asynq.NewServer(
		asynq.RedisClientOpt{Addr: "localhost:6379"},
		asynq.Config{Concurrency: 10},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc("check:serialization", checkSerializationOfBunDb)

	if err := srv.Run(mux); err != nil {
		log.Fatal(err)
	}
}

func checkSerializationOfBunDb(ctx context.Context, t *asynq.Task) error {
	var p CheckSerialization
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		log.Fatal(err.Error())
	}
	log.Println(&p.BunDb)
	// Check if BunDB field is initialized
	return nil

}
