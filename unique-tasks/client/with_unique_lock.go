package main

import (
	"github.com/hibiken/asynq"
	"log"
	"time"
)

// client.go
func main() {
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})

	t1 := asynq.NewTask("example", []byte("hello"))

	// t1 will hold the uniqueness lock for the next hour.

	info, err := client.Enqueue(t1, asynq.Unique(time.Hour))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

	t2 := asynq.NewTask("example", []byte("hello"))

	// t2 cannot be enqueued because it's a duplicate of t1.
	info, err = client.Enqueue(t2, asynq.Unique(time.Hour))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

}
