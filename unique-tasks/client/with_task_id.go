package main

import (
	"encoding/json"
	"github.com/hibiken/asynq"
	"log"
)

type User struct {
	ID int
}

// client.go
func main() {
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})
	payload, err := json.Marshal(User{ID: 42})

	task := asynq.NewTask("send_notification", payload)

	// Multiple same Task jobs
	info, err := client.Enqueue(task)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

	info, err = client.Enqueue(task)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

	// First task should be ok
	info, err = client.Enqueue(task, asynq.TaskID("1234"))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

	// Same task in different queue, is acceptable
	info, err = client.Enqueue(task, asynq.Queue("critical"), asynq.TaskID("unique_id"))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

	// Second task will fail, err is ErrTaskIDConflict (assuming that the first task didn't get processed yet)
	info, err = client.Enqueue(task, asynq.TaskID("unique_id"))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

}
