package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/hibiken/asynq"
	"github.com/slack-go/slack"
	"log"
)

type EmailTaskPayload struct {
	// ID for the email recipient.
	User string
}

func SendSlackNotification(webhookURL, message string) error {

	// Compose the message
	attachment := slack.Attachment{
		Color:    "good",
		Fallback: "You successfully posted by Incoming Webhook URL!",
		Text:     message,
		Footer:   "slack api",
	}
	msg := slack.WebhookMessage{
		Attachments: []slack.Attachment{attachment},
	}

	err := slack.PostWebhook(webhookURL, &msg)
	if err != nil {
		fmt.Println(err)
	}

	return nil
}

// workers.go
func main() {
	srv := asynq.NewServer(
		asynq.RedisClientOpt{Addr: "localhost:6379"},
		asynq.Config{Concurrency: 10},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc("email:welcome", sendWelcomeEmail)

	if err := srv.Run(mux); err != nil {
		log.Fatal(err)
	}
}

func sendWelcomeEmail(ctx context.Context, t *asynq.Task) error {
	var p EmailTaskPayload
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		SendSlackNotification(
			"https://hooks.slack.com/services/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",
			err.Error(),
		)
		return err
	}
	log.Printf(" [*] Send Welcome Email to User %d", p.User)
	SendSlackNotification(
		"https://hooks.slack.com/services/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",
		"Successfully sent the email")
	return nil
}
