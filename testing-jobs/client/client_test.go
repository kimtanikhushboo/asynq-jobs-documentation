package client_test

import (
	"context"
	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/onsi/ginkgo/v2"
	"github.com/onsi/gomega"
	"github.com/redis/go-redis/v9"
	client2 "quickstart/testing-jobs/client"
)

var _ = ginkgo.Describe("Client", func() {
	var (
		client      *asynq.Client
		redisClient *redis.Client
	)

	ginkgo.BeforeEach(func() {
		redisAddr := "localhost:6379"

		redisClient = redis.NewClient(&redis.Options{
			Addr: redisAddr,
		})

		client = asynq.NewClient(asynq.RedisClientOpt{
			Addr: redisAddr,
		})
	})
	defer client.Close()
	defer redisClient.Close()

	ginkgo.It("should enqueue a job in Redis", func() {
		taskType := "email:welcome"
		taskId := uuid.New().String()
		payload := client2.EmailTaskPayload{UserID: 12}
		queueName := "testing-jobs"

		err := client2.EnqueueJob(client, payload, taskType, taskId, queueName)

		gomega.Expect(err).NotTo(gomega.HaveOccurred())
		key := "asynq:{" + queueName + "}:t:" + taskId + ""

		_, err = redisClient.HGetAll(context.Background(), key).Result()

		gomega.Expect(err).NotTo(gomega.HaveOccurred())

	})
})
