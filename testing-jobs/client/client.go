package client

import (
	"encoding/json"
	"github.com/hibiken/asynq"
	"log"
)

// Task payload for any email related tasks.
type EmailTaskPayload struct {
	// ID for the email recipient.
	UserID int
}

func main() {
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})
	payload := EmailTaskPayload{UserID: 1}
	EnqueueJob(client, payload, "email:welcome", "1234", "testing")
}
func EnqueueJob(client *asynq.Client, payload EmailTaskPayload, taskType string, taskId string, queueName string) error {
	// Create a task with typename and payload.
	p, err := json.Marshal(payload)
	if err != nil {
		log.Fatal(err)
	}
	t1 := asynq.NewTask(taskType, p)

	// Process the task immediately.
	info, err := client.Enqueue(t1, asynq.TaskID(taskId), asynq.Queue(queueName))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)
	return nil

}
