package main

import (
	"encoding/json"
	"github.com/hibiken/asynq"
	"log"
)

type User struct {
	ID int
}

// client.go
func main() {
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})

	payload, err := json.Marshal(User{ID: 42})

	task := asynq.NewTask("send_notification", payload)

	// Specify a task to use "critical" queue using `asynq.Queue` option.
	info, err := client.Enqueue(task, asynq.Queue("critical"))
	if err != nil {
		log.Fatal(err)
	}

	log.Printf(" [*] Successfully enqueued task: %+v", info)

	// By default, task will be enqueued to "default" queue.
	info, err = client.Enqueue(task)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)

}
