package main

import (
	"context"
	"encoding/json"
	"github.com/hibiken/asynq"
	"log"
)

type User struct {
	// ID for the email recipient.
	ID int
}

// workers.go
func main() {
	srv := asynq.NewServer(
		asynq.RedisClientOpt{Addr: "localhost:6379"},
		asynq.Config{
			Concurrency: 10,
			Queues: map[string]int{
				"critical": 6,
				"default":  3,
				"low":      1,
			},
			//StrictPriority: true, // strict mode!

		},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc("send_notification", sendNotification)

	if err := srv.Run(mux); err != nil {
		log.Fatal(err)
	}

}

func sendNotification(ctx context.Context, t *asynq.Task) error {
	var user User
	if err := json.Unmarshal(t.Payload(), &user); err != nil {
		return err
	}
	log.Printf(" [*] Send Notification to User %d", user.ID)
	return nil
}
