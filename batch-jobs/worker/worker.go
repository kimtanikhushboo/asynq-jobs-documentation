package main

import (
	"context"
	"flag"
	"log"
	"strings"

	"github.com/hibiken/asynq"
)

// Simple aggregation function.
// Combines all tasks messages, each message on a separate line.
func aggregate(group string, tasks []*asynq.Task) *asynq.Task {
	log.Printf("Aggregating %d tasks from group %q", len(tasks), group)
	var b strings.Builder
	for _, t := range tasks {
		b.Write(t.Payload())
		b.WriteString("\n")
	}
	return asynq.NewTask("aggregated-task", []byte(b.String()))
}

func handleAggregatedTask(ctx context.Context, task *asynq.Task) error {
	log.Print("Handler received aggregated task")
	log.Printf("aggregated messags: %s", task.Payload())
	return nil
}

func main() {
	flag.Parse()

	srv := asynq.NewServer(
		asynq.RedisClientOpt{Addr: "localhost:6379"},
		asynq.Config{
			Queues:          map[string]int{"tutorial": 1},
			GroupAggregator: asynq.GroupAggregatorFunc(aggregate),
		},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc("aggregated-task", handleAggregatedTask)

	if err := srv.Run(mux); err != nil {
		log.Fatalf("Failed to start the server: %v", err)
	}
}
