package main

import (
	"flag"
	"github.com/hibiken/asynq"
	"log"
)

func main() {
	flag.Parse()

	c := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})
	defer c.Close()

	task1 := asynq.NewTask("aggregation-tutorial", []byte("this is task 1"))
	task2 := asynq.NewTask("aggregation-tutorial", []byte("this is task 2"))
	task3 := asynq.NewTask("aggregation-tutorial", []byte("this is task 3"))

	info, err := c.Enqueue(task1, asynq.Queue("tutorial"), asynq.Group("example-group"))
	if err != nil {
		log.Fatalf("Failed to enqueue task 1: %v", err)
	}
	log.Printf("Successfully enqueued tasks 1 : %s", info.ID)

	info, err = c.Enqueue(task2, asynq.Queue("tutorial"), asynq.Group("example-group"))
	if err != nil {
		log.Fatalf("Failed to enqueue task 2: %v", err)
	}
	log.Printf("Successfully enqueued tasks 2 : %s", info.ID)

	info, err = c.Enqueue(task3, asynq.Queue("tutorial"), asynq.Group("example-group"))

	if err != nil {
		log.Fatalf("Failed to enqueue task 3: %v", err)
	}
	log.Printf("Successfully enqueued tasks 3 : %s", info.ID)

}
