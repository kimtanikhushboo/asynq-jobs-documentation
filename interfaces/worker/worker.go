package main

import (
	"context"
	"encoding/json"
	"github.com/hibiken/asynq"
	"log"
)

type Payload interface {
	GetPayload() interface{}
}

type CustomPayload struct {
	Data interface{}
}

func (p CustomPayload) GetPayload() interface{} {
	return p.Data
}

type GenericPayload struct {
	StructName string  `json:"struct_name"`
	Payload    Payload `json:"payload"`
}

type EmailTaskPayload struct {
	UserId int `json:"user_id"`
}

// workers.go
func main() {
	srv := asynq.NewServer(
		asynq.RedisClientOpt{Addr: "localhost:6379"},
		asynq.Config{Concurrency: 10},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc("email:welcome", sendWelcomeEmail)

	if err := srv.Run(mux); err != nil {
		log.Fatal(err)
	}
}

func sendWelcomeEmail(ctx context.Context, t *asynq.Task) error {
	var p GenericPayload
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		return err
	}
	log.Println(p.StructName)
	return nil

}
