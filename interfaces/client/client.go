package main

import (
	"encoding/json"
	"github.com/hibiken/asynq"
	"log"
)

type Payload interface {
	GetPayload() interface{}
}

// Define a struct for custom payload.
type CustomPayload struct {
	Data interface{}
}

// Implement the Payload interface for the CustomPayload struct.
func (p CustomPayload) GetPayload() interface{} {
	return p.Data
}

// Define a struct to hold the generic payload.
type GenericPayload struct {
	StructName string      `json:"struct_name"`
	Payload    interface{} `json:"payload"`
}

type EmailTaskPayload struct {
	UserId int `json:"user_id"`
}

func main() {
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: "localhost:6379"})
	customPayloadStr := CustomPayload{Data: EmailTaskPayload{UserId: 42}}

	EnqueueJob(client, "EmailTaskPayload", customPayloadStr)

}

func EnqueueJob(client *asynq.Client, customPayloadStructName string, customPayload CustomPayload) {
	genericPayloadStr := GenericPayload{
		StructName: customPayloadStructName,
		Payload:    customPayload,
	}
	log.Println(genericPayloadStr.Payload)

	// Create a task with typename and payload.
	payload, err := json.Marshal(genericPayloadStr)
	if err != nil {
		log.Fatal(err)
	}
	t1 := asynq.NewTask("email:welcome", payload, asynq.TaskID("1234"))

	// Process the task immediately.
	info, err := client.Enqueue(t1)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf(" [*] Successfully enqueued task: %+v", info)
}
